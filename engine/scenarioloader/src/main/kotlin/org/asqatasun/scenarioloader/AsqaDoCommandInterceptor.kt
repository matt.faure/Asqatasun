package org.asqatasun.scenarioloader

import jp.vmi.selenium.selenese.Context
import jp.vmi.selenium.selenese.command.Echo
import jp.vmi.selenium.selenese.command.ICommand
import jp.vmi.selenium.selenese.inject.AbstractDoCommandInterceptor
import jp.vmi.selenium.selenese.result.Result
import org.aopalliance.intercept.MethodInvocation

class AsqaDoCommandInterceptor : AbstractDoCommandInterceptor() {

    companion object {
        private val scenarioLoaderMap = HashMap<Long, ScenarioLoader>()
        fun addScenarioLoader(idAudit: Long, scenarioLoader: ScenarioLoader) =
            scenarioLoaderMap.put(idAudit, scenarioLoader)
        fun removeScenarioLoader(idAudit: Long) = scenarioLoaderMap.remove(idAudit)
    }

    override fun invoke(
        invocation: MethodInvocation?,
        context: Context?,
        command: ICommand?,
        curArgs: Array<out String>? ) =
        (invocation!!.proceed() as Result).also {
            scenarioLoaderMap[context!!.varsMap["AUDIT"]].also {
                it?.proceedNewResult(command is Echo && (command).arguments[0].equals("audit", true))
            }
        }
}
