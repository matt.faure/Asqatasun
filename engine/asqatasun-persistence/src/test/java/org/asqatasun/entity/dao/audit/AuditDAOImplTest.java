/*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.entity.dao.audit;

import org.asqatasun.entity.audit.Audit;
import org.asqatasun.entity.audit.AuditStatus;
import org.asqatasun.entity.dao.test.AbstractDaoTestCase;
import org.asqatasun.entity.subject.Page;
import org.asqatasun.entity.subject.Site;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collection;

/**
 * @author jkowalczyk
 */
@Profile("test")
@ActiveProfiles("test")
public class AuditDAOImplTest extends AbstractDaoTestCase {

    private static final String INPUT_DATA_SET_FILENAME = "auditFlatXmlDataSet.xml";

    @Autowired
    private AuditDAO auditDAO;

    public AuditDAOImplTest() {
        super();
    }

    @Override
    protected String getDataSetFilename() {
        return getInputDataFilePath() + INPUT_DATA_SET_FILENAME;
    }

    @Test
    public void testFindAll_AuditStatus() {
        Collection<Audit> auditList = auditDAO.findAll(AuditStatus.COMPLETED);
        assertEquals(2, auditList.size());
        auditList = auditDAO.findAll(AuditStatus.ANALYSIS);
        assertEquals(0, auditList.size());
    }

    @Test
    public void testRead() {
        Audit audit = auditDAO.read(1L);
        assertEquals(Long.valueOf(1), audit.getId());
        assertTrue(audit.getSubject() instanceof Site);
        assertEquals(Long.valueOf(1), audit.getSubject().getId());
        audit = auditDAO.read(2L);
        assertEquals(Long.valueOf(2), audit.getId());
        assertEquals(Long.valueOf(2), audit.getSubject().getId());
        assertTrue(audit.getSubject() instanceof Page);
    }

    @Test
    public void testFindAllByTags() {
        Collection<Audit> auditList = auditDAO.findAllByTag("tagA");
        assertEquals(1, auditList.size());
        assertEquals(Long.valueOf(1), auditList.stream().findFirst().get().getId());

        auditList = auditDAO.findAllByTag("tagB");
        assertEquals(2, auditList.size());
    }

    @Test
    public void testFindAuditWithTest() {
        Audit audit = auditDAO.findAuditWithTest(1L);
        assertEquals(Long.valueOf(1), audit.getId());
        assertEquals(10, audit.getTestList().size());
        audit = auditDAO.findAuditWithTest(2L);
        assertEquals(Long.valueOf(2), audit.getId());
        assertEquals(10, audit.getTestList().size());
    }

}
