/*
 * Asqatasun - Automated webpage assessment
 * Copyright (C) 2008-2020  Asqatasun.org
 *
 * This file is part of Asqatasun.
 *
 * Asqatasun is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact us by mail: asqatasun AT asqatasun DOT org
 */
package org.asqatasun.webapp.report.layout.builder;

import org.apache.commons.text.StringEscapeUtils;
import org.asqatasun.webapp.dto.AuditStatistics;

import java.util.*;

/**
 *
 * @author jkowalczyk
 */
public class SubtitleBuilderImpl implements TitleBuilder {

    private static final char PERCENT_KEY = '%';
    private static final String SEPARATOR_KEY = "  -  ";
    private static final String DOUBLE_DOT_KEY = " : ";
    private static final char SPACE_KEY = ' ';

    private static final String MARK_KEY = "export-report.mark";
    private static final String REF_KEY = "referential";
    private static final String LEVEL_KEY = "level";
    
    private String levelParamKey = "LEVEL";

    public void setLevelParamKey(String levelParamKey) {
        this.levelParamKey = levelParamKey;
    }

    private String bundleName;

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    private String levelBundleName;

    public void setLevelBundleName(String levelBundleName) {
        this.levelBundleName = levelBundleName;
    }

    private String refBundleName;

    public void setRefBundleName(String refBundleName) {
        this.refBundleName = refBundleName;
    }
    
    private final List<String> refAndLevelValueBundleNameList = new ArrayList<>();

    public void setRefAndLevelValueBundleList(List<String> refAndLevelValueBundleNameList) {
        this.refAndLevelValueBundleNameList.addAll(refAndLevelValueBundleNameList);
    }

    @Override
    public String getTitle(AuditStatistics auditStatistics, Locale locale) {
        ResourceBundle bundle = ResourceBundle.getBundle(bundleName, locale);
        return bundle.getString(MARK_KEY) +
            SPACE_KEY +
            auditStatistics.getRawMark() +
            PERCENT_KEY +
            SEPARATOR_KEY +
            getRefAndLevel(auditStatistics, locale);
    }

    private String getRefAndLevel(AuditStatistics auditStatistics, Locale locale) {
        ResourceBundle refBundle = ResourceBundle.getBundle(refBundleName, locale);
        Collection<ResourceBundle> refAndlevelValueBundleList = new ArrayList<>();
        for (String bundle: refAndLevelValueBundleNameList) {
            refAndlevelValueBundleList.add(ResourceBundle.getBundle(bundle, locale));
        }
        ResourceBundle levelBundle = ResourceBundle.getBundle(levelBundleName, locale);
        return StringEscapeUtils.unescapeHtml4(refBundle.getString(REF_KEY)) +
            DOUBLE_DOT_KEY +
            StringEscapeUtils.unescapeHtml4(
                retrieveI18nValue(
                    auditStatistics.getParametersMap().get(REF_KEY)+"-optgroup",
                    refAndlevelValueBundleList)) +
            SEPARATOR_KEY +
            StringEscapeUtils.unescapeHtml4(levelBundle.getString(LEVEL_KEY)) +
            DOUBLE_DOT_KEY +
            StringEscapeUtils.unescapeHtml4(
                retrieveI18nValue(
                    auditStatistics.getParametersMap().get(LEVEL_KEY).replace(";", "-"),
                    refAndlevelValueBundleList));
    }

    /**
     * Retrieve a i18n among the Collection of resourceBundle associated with
     * the instance
     * 
     * @param key
     * @param resourceBundleList
     * @return 
     */
    private String retrieveI18nValue(String key, Collection<ResourceBundle> resourceBundleList) {
        for (ResourceBundle rb: resourceBundleList) {
            if (rb.containsKey(key)) {
                return rb.getString(key);
            }
        }
        return key;
    }
}
