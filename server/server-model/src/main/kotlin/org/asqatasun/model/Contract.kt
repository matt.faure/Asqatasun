package org.asqatasun.model

import io.swagger.v3.oas.annotations.media.Schema
import org.asqatasun.entity.contract.Contract
import org.asqatasun.entity.functionality.Functionality
import org.asqatasun.entity.option.Option
import org.asqatasun.entity.option.OptionElement
import org.asqatasun.entity.option.OptionElementImpl
import org.asqatasun.entity.service.contract.ContractDataService
import org.asqatasun.entity.service.functionality.FunctionalityDataService
import org.asqatasun.entity.service.option.OptionDataService
import org.asqatasun.entity.service.option.OptionElementDataService
import org.asqatasun.entity.service.referential.ReferentialDataService
import org.asqatasun.entity.service.user.UserDataService
import java.util.*

@Schema(
    title = "ContractDto",
    description = "A contract represents the functionalities opened for a given user"
)
data class ContractDto (
    @field:Schema(
        description = "Unique id of the contract",
        example = "42",
    )
    val id: Long,
    @field:Schema(
        description = "The displayable label of the contract",
        example = "My favourite website",
    )
    val label: String,
    val user: UserDto,
    @field:Schema(
        description = "The begin date of the contract"
    )
    val beginDate: Date?,
    @field:Schema(
        description = "The end date of the contract"
    )
    val endDate: Date?,
    @field:Schema(
        description = "The list of act (audits) launched by this contract"
    )
    val actSet: List<ActDto>,
    @field:Schema(
        description = "The list of selenium scenario associated with this contract"
    )
    val functionalities: List<String>,
    var optionElements: Map<String, String>,
    var referentials: List<Referential>
)

@Schema(
    title = "ContractAddDto"
)
data class ContractAddDto(
    @field:Schema(
        description = "The user id the contract will be associated with",
        example = "59",
    )
    val userId: Long,
    val label: String,
    @field:Schema(
        description = "The begin date of the contract"
    )
    val beginDate: Date,
    @field:Schema(
        description = "The end date of the contract"
    )
    val endDate: Date?,
    @field:Schema(
        description = "The list of authorized functionalities",
        allowableValues = ["PAGES", "DOMAIN", "MANUAL", "UPLOAD", "SCENARIO"]
    )
    val functionalities: List<String>,
    @field:Schema(
        description = "The list of authorized options",
        allowableValues = ["ACT_LIMITATION", "ACT_BY_IP_LIMITATION", "MAX_DOCUMENTS", "FORDIDDEN_REFERENTIAL",
            "DEPTH", "MAX_DURATION", "EXCLUSION_REGEXP", "ACT_LIFETIME", "NB_OF_AUDIT_TO_DISPLAY", "DOMAIN",
            "PAGES_ACT_LIMITATION", "DOMAIN_ACT_LIMITATION", "UPLOAD_ACT_LIMITATION", "SCENARIO_ACT_LIMITATION",
            "PRESET_CONTRACT", "ROBOTS_TXT_ACTIVATION"]
    )
    val options: Map<String, String>,
    @field:Schema(
        description = "The list of authorized referentials",
        allowableValues = ["RGAA_4_0", "RGAA_3_0", "SEO", "ACCESSIWEB_2_2"]
    )
    val referentials: List<String>
)

@Schema(
    title = "ContractUpdateDto"
)
data class ContractUpdateDto (
    @field:Schema(
        description = "The id of the modified contract",
        example = "42",
    )
    val id: Long,
    @field:Schema(
        description = "The user id the contract will be associated with",
        example = "59",
    )
    val userId: Long,
    @field:Schema(
        description = "The displayable label of the contract",
        example = "My favourite website",
    )
    val label: String,
    @field:Schema(
        description = "The begin date of the contract"
    )
    val beginDate: Date,
    @field:Schema(
        description = "The end date of the contract"
    )
    val endDate: Date?,
    @field:Schema(
        description = "The list of authorized functionalities",
        allowableValues = ["PAGES", "DOMAIN", "MANUAL", "UPLOAD", "SCENARIO"]
    )
    val functionalities: List<String>,
    @field:Schema(
        description = "The list of authorized options",
        allowableValues = ["ACT_LIMITATION", "ACT_BY_IP_LIMITATION", "MAX_DOCUMENTS", "FORDIDDEN_REFERENTIAL",
            "DEPTH", "MAX_DURATION", "EXCLUSION_REGEXP", "ACT_LIFETIME", "NB_OF_AUDIT_TO_DISPLAY", "DOMAIN",
            "PAGES_ACT_LIMITATION", "DOMAIN_ACT_LIMITATION", "UPLOAD_ACT_LIMITATION", "SCENARIO_ACT_LIMITATION",
            "PRESET_CONTRACT", "ROBOTS_TXT_ACTIVATION"]
    )
    val options: Map<String, String>,
    @field:Schema(
        description = "The list of authorized referentials",
        allowableValues = ["RGAA_4_0", "RGAA_3_0", "SEO", "ACCESSIWEB_2_2"]
    )
    val referentials: List<String>
)

fun Contract.toContractDto() = ContractDto (
    id,
    label,
    user.toUserDto(),
    beginDate,
    endDate,
    actSet.map { act -> act.toActDto() },
    functionalitySet.map { functionality -> functionality.code },
    optionElementSet.associate { optionElement -> Pair(optionElement.option.code, optionElement.value) },
    referentialSet.map {ref ->  Referential.fromCode(ref.code.split(';')[0])!!}
)

fun ContractAddDto.toContract(userDataService: UserDataService,
                              functionalityDataService: FunctionalityDataService,
                              optionDataService: OptionDataService,
                              optionElementDataService: OptionElementDataService,
                              referentialDataService: ReferentialDataService) : Contract {
    val contract = Contract()
    return setContractData(
        contract,
        userId,
        label,
        beginDate,
        endDate,
        functionalities,
        options,
        referentials,
        userDataService,
        functionalityDataService,
        optionDataService,
        optionElementDataService,
        referentialDataService)
}

fun ContractUpdateDto.toContract(userDataService: UserDataService,
                                 contractDataService: ContractDataService,
                                 functionalityDataService: FunctionalityDataService,
                                 optionDataService: OptionDataService,
                                 optionElementDataService: OptionElementDataService,
                                 referentialDataService: ReferentialDataService) : Contract {
    val contract = contractDataService.read(id) ?: throw ContractNotFoundException(id)
    return setContractData(
        contract,
        userId,
        label,
        beginDate,
        endDate,
        functionalities,
        options,
        referentials,
        userDataService,
        functionalityDataService,
        optionDataService,
        optionElementDataService,
        referentialDataService)
}

fun setContractData(contract: Contract,
                    userId: Long,
                    label: String,
                    beginDate: Date,
                    endDate: Date?,
                    functionalities: List<String>,
                    options: Map<String, String>,
                    referentials: List<String>,
                    userDataService: UserDataService,
                    functionalityDataService: FunctionalityDataService,
                    optionDataService: OptionDataService,
                    optionElementDataService: OptionElementDataService,
                    referentialDataService: ReferentialDataService): Contract {
    contract.user = userDataService.read(userId) ?: throw UserNotFoundException(userId)
    contract.label = label
    contract.beginDate = beginDate
    contract.endDate = endDate
    contract.addAllFunctionality(
        functionalities.map{ refCode ->
            functionalityDataService.getByCode(refCode)
                ?: throw FunctionalityNotFoundException(functionalities, functionalityDataService.findAll())
        }.toSet()
    )
    contract.addAllOptionElement(
        options.map { entry ->
            val option = optionDataService.getOption(entry.key)
                ?: throw OptionNotFoundException(entry.key, optionDataService.findAll())
            var optionElement = optionElementDataService.getOptionElementFromValueAndOption(entry.value, option)
            if (optionElement == null) {
                optionElement = OptionElementImpl()
                optionElement.value = entry.value
                optionElement.option = option
                optionElement = optionElementDataService.saveOrUpdate(optionElement)
            }
            optionElement
        }
    )
    contract.addAllReferential(
        referentials.map { refCode ->
            try {
                referentialDataService.getByCode(Referential.valueOf(refCode).code)
                    ?: throw ReferentialNotFoundException(referentials)
            } catch (iae: IllegalArgumentException) {
                throw ReferentialNotFoundException(referentials)
            }
        }.toSet()
    )
    return contract;
}

class UserNotFoundException(userId: Long): Exception("Cannot find user with id: $userId")
class ContractNotFoundException(id: Long): Exception("Cannot find contract with id: $id")
class FunctionalityNotFoundException(functionalities: List<String>, possibleValues: MutableCollection<Functionality>):
    Exception("At least one functionality does not exist among ${functionalities.joinToString()}. " +
        "Possible values are ${possibleValues.joinToString { functionality -> functionality.code }}")

class ReferentialNotFoundException(functionalities: List<String>):
    Exception("At least one referential does not exist among ${functionalities.joinToString()}. " +
        "Possible values are ${Referential.entries.joinToString { entry -> entry.name }}")

class OptionNotFoundException(optionCode: String, options: MutableCollection<Option>):
    Exception("The following option '$optionCode' does not exist. " +
        "Possible values are ${options.joinToString { option -> option.code }}")
