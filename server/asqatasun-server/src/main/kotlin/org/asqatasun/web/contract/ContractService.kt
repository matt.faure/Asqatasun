package org.asqatasun.web.contract

import org.asqatasun.entity.service.contract.ContractDataService
import org.asqatasun.entity.service.functionality.FunctionalityDataService
import org.asqatasun.entity.service.option.OptionDataService
import org.asqatasun.entity.service.option.OptionElementDataService
import org.asqatasun.entity.service.referential.ReferentialDataService
import org.asqatasun.entity.service.user.UserDataService
import org.asqatasun.model.ContractAddDto
import org.asqatasun.model.ContractUpdateDto
import org.asqatasun.model.toContract
import org.asqatasun.model.toContractDto
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class ContractService(private val contractDataService: ContractDataService,
                      private val userDataService: UserDataService,
                      private val functionalityDataService: FunctionalityDataService,
                      private val optionDataService: OptionDataService,
                      private val optionElementDataService: OptionElementDataService,
                      private val referentialDataService: ReferentialDataService) {

    fun getAllContract(username :String?) =
        username?.let {
            contractDataService.getAllContractsByUser(userDataService.getUserFromEmail(username))
                .map { contract -> contract.toContractDto() }}
            ?: contractDataService.findAll().map { contract -> contract.toContractDto() }

    fun getContractFromId(id :Long) =
        Optional.ofNullable(contractDataService.read(id)).map { contract -> contract.toContractDto()}
            .orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "Contract Not Found")
            }!!

    fun deleteContractFromId(id :Long) =
        Optional.ofNullable(contractDataService.delete(id))
            .orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "Contract Not Found")
            }!!

    fun addContract(addContractDto : ContractAddDto): Long? =
        contractDataService.saveOrUpdate(
            addContractDto.toContract(
                userDataService,
                functionalityDataService,
                optionDataService,
                optionElementDataService,
                referentialDataService))
            .id

    fun updateContract(updateContractDto : ContractUpdateDto): Long? =
        contractDataService.saveOrUpdate(
            updateContractDto.toContract(
                userDataService,
                contractDataService,
                functionalityDataService,
                optionDataService,
                optionElementDataService,
                referentialDataService))
            .id
}
