package org.asqatasun.web.user

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl
import org.springframework.stereotype.Service
import jakarta.annotation.PostConstruct
import javax.sql.DataSource

@Service
class UserDetailsService(private val asqaDataSource: DataSource) : JdbcDaoImpl() {

    @PostConstruct
    fun init() {
        dataSource = asqaDataSource
        usersByUsernameQuery = USERS_BY_USERNAME_QUERY
        authoritiesByUsernameQuery = AUTHORITIES_BY_USERNAME_QUERY
    }

    override fun createUserDetails(username: String, userFromUserQuery: UserDetails,
                                   combinedAuthorities: List<GrantedAuthority>): UserDetails {
        return User(username, userFromUserQuery.password, combinedAuthorities)
    }

    companion object {
        private const val USERS_BY_USERNAME_QUERY = "SELECT Email1, Password, Activated as enabled FROM USERS WHERE Email1=?"
        private const val AUTHORITIES_BY_USERNAME_QUERY = ("SELECT USERS.Email1, ROLE.Role_Name as authorities FROM USERS " +
            "left join ROLE on USERS.role_id_role = ROLE.id_role " +
            "WHERE USERS.Email1 = ?")
    }
}
